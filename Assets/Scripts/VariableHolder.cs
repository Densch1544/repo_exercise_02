﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{

    public class VariableHolder : MonoBehaviour
    {
        private string playerName = "Dennis";
        private int playerAttack = 20;
        private int playerDefense = 10;
        private float playerSpeed = 15.5f;
        private bool isPlayerDead = false;

        public bool isPlayerPoisoned = false;
        public bool isPlayerBleeding = true;
        public int playerHealth = 100;
        public int playerMana = 200;
        public string playerTitle = "Your Majesty";




        public void Start()
        {
        ShowAll();
        }
        
        public void ShowAll()
        {
            Debug.Log(playerName);
            Debug.Log(playerAttack);
            Debug.Log(playerDefense);
            Debug.Log(playerSpeed);
            Debug.Log(isPlayerDead);
            Debug.Log(isPlayerPoisoned);
            Debug.Log(isPlayerBleeding);
            Debug.Log(playerHealth);
            Debug.Log(playerMana);
            Debug.Log(playerTitle);
        }
    }
}